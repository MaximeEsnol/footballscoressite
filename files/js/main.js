
/**
 * Opens the file and sends the contents of the file to the callback function which will 
 * handle what happens to the data.
 * @param {String} url The URL or the local path of the file to be opened.
 * @param {*} callback The function that will handle the data contained in the file.
 */
function open(url, callback){
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = function(){
            if(xhr.readyState == 4 && xhr.status == 200){
                  callback(xhr.response);
            }
      }

      xhr.open('GET', url);
      xhr.send();
}

/**
 * Loads the data in the DATA.JSON file.
 */
function loadData(){
      open("data.json", showMatches);
}

function showMatches(res){
      var data = JSON.parse(res);
      var matches = data.matches;

      var table = document.getElementById('matches_body');

      for(var match of matches){
            var tr = document.createElement('tr');
            var dateElem = document.createElement('td');
            var date = document.createTextNode(match.date);
            dateElem.appendChild(date);

            var homeTeamElem = document.createElement('td');
            var homeTeam = document.createTextNode(match.home_team);
            homeTeamElem.appendChild(homeTeam);

            var homeScoreElem = document.createElement('td');
            var homeScore = document.createTextNode(match.home_score);
            homeScoreElem.appendChild(homeScore);

            var awayScoreElem = document.createElement('td');
            var awayScore = document.createTextNode(match.away_score);
            awayScoreElem.appendChild(awayScore);

            var awayTeamElem = document.createElement('td');
            var awayTeam = document.createTextNode(match.away_team);
            awayTeamElem.appendChild(awayTeam);

            tr.appendChild(dateElem);
            tr.appendChild(homeTeamElem);
            tr.appendChild(homeScoreElem);
            tr.appendChild(awayScoreElem);
            tr.appendChild(awayTeamElem);
            table.appendChild(tr);
      }

      document.getElementById('matches_body_preloader').remove();
}


window.onload = function(){
      this.loadData();
}